import 'package:flutter/foundation.dart';

class Thread {
  final String id;
  final String title;
  final String imageUrl;

  const Thread({
    @required this.id,
    @required this.title,
    @required this.imageUrl,
  });
}
