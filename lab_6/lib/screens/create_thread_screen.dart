import 'dart:ui';

import 'package:flutter/material.dart';

import '../models/thread.dart';

class CreateThreadScreen extends StatefulWidget {
  static const routeName = '/create-thread';

  final List<Thread> threader;

  CreateThreadScreen(this.threader);

  @override
  _CreateThreadScreenState createState() => _CreateThreadScreenState();
}

class _CreateThreadScreenState extends State<CreateThreadScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Image(
        image: AssetImage('../assets/images/popowi.jpg'),
        width: 300,
        height: 300,
      ),
    );
  }
}
