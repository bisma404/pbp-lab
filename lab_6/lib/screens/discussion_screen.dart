import 'package:flutter/material.dart';

import '../models/thread.dart';
import '../widgets/meal_item.dart';
import '../screens/create_thread_screen.dart';

class DiscussionScreen extends StatelessWidget {
  final List<Thread> threader;

  DiscussionScreen(this.threader);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: threader.isEmpty
          ? Center(
              child: Text('Mulai diskusi baru sekarang!'),
            )
          : ListView.builder(
              itemBuilder: (ctx, index) {
                return MealItem(
                  id: threader[index].id,
                  title: threader[index].title,
                  imageUrl: threader[index].imageUrl,
                );
              },
              itemCount: threader.length,
            ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.create,
        ),
        onPressed: () => Navigator.of(context)
            .pushReplacementNamed(CreateThreadScreen.routeName),
      ),
    );
  }
}
