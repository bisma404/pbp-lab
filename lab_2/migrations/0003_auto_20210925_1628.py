# Generated by Django 3.2.7 on 2021-09-25 09:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0002_auto_20210925_1555'),
    ]

    operations = [
        migrations.RenameField(
            model_name='note',
            old_name='Message',
            new_name='message',
        ),
        migrations.RenameField(
            model_name='note',
            old_name='Title',
            new_name='title',
        ),
        migrations.RenameField(
            model_name='note',
            old_name='To',
            new_name='to',
        ),
    ]
