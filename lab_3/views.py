from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from .forms import FriendForm

@login_required(login_url='/admin/login/')
# Create your views here.
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}

    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)

    # check if request method is POST and form data is valid  
    if request.method == "POST" and form.is_valid():
        # save the form data to model
        form.save()
        return redirect('/lab-3/')

    context['form'] = form
    return render(request, "lab3_form.html", context)