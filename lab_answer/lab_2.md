1. Apakah perbedaan antara JSON dan XML?
---

    **JSON**

    JSON (JavaScript Object Notation) tersusun atas JavaScript language. JSON dimanfaatkan untuk merepresentasikan objects. Dibandingkan XML, file
    
    JSON lebih mudah untuk dibaca. Namun, hal ini menyebabkan JSON kurang aman dibandingkan XML. Selain itu, JSON tidak menggunakan end tag untuk
    
    mengakhiri code. JSON hanya support UTF-8 encoding, JSON juga support penggunaan array. Di sisi lain, JSON tidak support namespaces dan
    
    comments.

    **XML**

    XML (eXtensible Markup Language) tersusun atas SGML (Standard Generalized Markup Language). XML merupakan markup language yang memanfaatkan
    
    struktur tag untuk merepresentasikan data items. Dibandingkan JSON, dokumen XML lebih sulit untuk dibaca dan dipahami. Namun, hal ini
    
    menyebabkan XML lebih aman dibandingkan JSON. Selain itu, XML mewajibkan start tag untuk memulai code dan end tag untuk mengakhiri code.
    
    XML support beragam jenis encoding, XML juga support namespaces dan comments. Di sisi lain, XML tidak support penggunaan array.

Reference: https://www.geeksforgeeks.org/difference-between-json-and-xml/

2. Apakah perbedaan antara HTML dan XML?
---

    **HTML**

    HTML adalah sebuah markup language yang bersifat Static dan tidak case sensitive. HTML dimanfaatkan untuk menampilkan data karena dapat
    
    menampilkan design suatu web page sebagaimana ditampilkan secara client-side. White spaces tidak dapat disimpan di dalam HTML. Selain itu, HTML
    
    memiliki tag khas-nya tersendiri, sehingga closing tags tidak terlalu dibutuhkan. HTML juga merangkap sebagai presentation language.

    **XML**

    XML adalah sebuah markup language standar yang mendefinisikan markup language lainnya. XML bersifat Dynamic dan case sensitive. XML dimanfaatkan
    
    untuk melakukan transfer data karena memungkinkan transportasi data dari database dan aplikasi terkait. Berbeda dengan HTML, XML dapat menyimpan
    
    white spaces. Pada XML, pembuatan tags bersifat fleksibel karena dapat disesuaikan dengan kebutuhan programmer. Di sisi lain, diwajibkan dalam
    
    coding XML untuk menggunakan closing tags. XML tidak termasuk bagian daripada presentation language maupun programming language.

Reference: https://www.upgrad.com/blog/html-vs-xml/
